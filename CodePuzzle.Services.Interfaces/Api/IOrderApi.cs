﻿using CodePuzzle.Domain.Models;
using System.Threading.Tasks;

namespace CodePuzzle.Services.Interfaces.Api
{
    public interface IOrderApi
    {
        Task<bool> SubmitOrder(Order order);
    }
}
