﻿using CodePuzzle.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodePuzzle.Services.Interfaces.Api
{
    public interface IProductApi
    {
        Task<IEnumerable<Product>> Get();
    }
}
