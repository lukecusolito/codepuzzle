﻿using CodePuzzle.Services.Interfaces.Api;

namespace CodePuzzle.Services.Interfaces.Services
{
    public interface IAllTheCloudsApiService
    {
        ICurrencyApi Currency { get; }
        IOrderApi Order { get; }
        IProductApi Product { get; }
    }
}
