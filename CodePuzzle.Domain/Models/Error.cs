﻿using CodePuzzle.Domain.Enumerations;

namespace CodePuzzle.Domain.Models
{
    public class Error
    {
        public ErrorCode ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
