﻿using System.Collections.Generic;

namespace CodePuzzle.Domain.Models
{
    public class Order
    {
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public List<OrderLineItem> LineItems { get; set; } = new List<OrderLineItem>();
    }
}
