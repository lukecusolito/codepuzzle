﻿using CodePuzzle.Domain.Enumerations;
using CodePuzzle.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CodePuzzle.Domain.Models
{
    public class ErrorResponse
    {
        #region Constructor(s)
        public ErrorResponse() { }

        public ErrorResponse(ErrorCode errorCode)
        {
            AddError(errorCode);
        }

        public ErrorResponse(List<ErrorCode> errorCodes)
        {
            foreach (var errorCode in errorCodes)
                AddError(errorCode);
        }
        #endregion

        #region Properties
        public List<Error> Errors { get; set; } = new List<Error>();
        #endregion

        #region Public Methods
        public void AddError(ErrorCode errorCode)
        {
            if (!Errors.Any(x => x.ErrorCode == errorCode))
                Errors.Add(new Error
                {
                    ErrorCode = errorCode,
                    ErrorMessage = LocalisationMessage.GetErrorMessage(errorCode)
                });
        }

        public bool HasErrors() =>
           Errors.Count > 0;

        public ErrorCode First()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
