﻿namespace CodePuzzle.Domain.Models
{
    public class OrderLineItem
    {
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
