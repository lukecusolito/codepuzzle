﻿using CodePuzzle.Domain.Enumerations;
using System.Collections.Generic;

namespace CodePuzzle.Domain.Models
{
    public class Metadata
    {
        public Currency DefaultCurrency { get; set; }
        public List<ForeignExchangeRate> ExchangeRates { get; set; }
    }
}
