﻿namespace CodePuzzle.Domain.Models.Configuration
{
    public class AllTheCloudsApiConfiguration
    {
        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
    }
}
