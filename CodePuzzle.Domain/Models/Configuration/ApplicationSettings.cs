﻿using CodePuzzle.Domain.Enumerations;

namespace CodePuzzle.Domain.Models.Configuration
{
    public class ApplicationSettings
    {
        public Currency DefaultCurrency { get; set; }
        public decimal ProductMarkup { get; set; }
    }
}
