﻿using System;

namespace CodePuzzle.Domain.Models
{
    public class Product
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public int? MaximumQuantity { get; set; }

        public void AddProductMarkup(decimal markupRate)
        {
            var markupValue = UnitPrice + (UnitPrice * markupRate);

            UnitPrice = Math.Round(markupValue, 2, MidpointRounding.AwayFromZero);
        }
    }
}
