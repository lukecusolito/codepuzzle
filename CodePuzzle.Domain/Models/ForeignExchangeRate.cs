﻿namespace CodePuzzle.Domain.Models
{
    public class ForeignExchangeRate
    {
        public string SourceCurrency { get; set; }
        public string TargetCurrency { get; set; }
        public decimal Rate { get; set; }
    }
}
