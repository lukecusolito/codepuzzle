﻿namespace CodePuzzle.Domain.Enumerations
{
    public enum ErrorCode
    {
        UNEXPECTED_ERROR,
        PRODUCTS_NOT_FOUND
    }
}
