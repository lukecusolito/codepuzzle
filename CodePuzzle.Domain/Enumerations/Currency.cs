﻿namespace CodePuzzle.Domain.Enumerations
{
    public enum Currency
    {
        AUD,
        USD,
        GBP
    }
}
