# Code Puzzle
Solution written using .NETCore and Vue.js

## Debugging
API project can be debugged via `.\CodePuzzle.sln`
UI project operated independantly from Visual Studio, follow instructions in `./code-puzzle-ui/README.md`

## NOTE: IDE Compatibility
`code-puzzle-ui` VisualStudio project for Vue.js is only compatible with VS2019, a project has not been supplied. Source code can be modified via VSCode.