import Vue from 'vue'
import App from './App.vue'
import Api from './api';
import GlobalComponents from './global-component-registry';

Vue.config.productionTip = false

Vue.use(GlobalComponents);

Vue.prototype.$api = Api;

new Vue({
  render: h => h(App),
}).$mount('#app')
