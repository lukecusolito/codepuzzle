import Api from '../base-class';

class ProductsaApi extends Api {
    controller = 'Product';
    
    retrieveProducts = () => (
        this._get(this.controller + '/')
    )
}

export default new ProductsaApi();