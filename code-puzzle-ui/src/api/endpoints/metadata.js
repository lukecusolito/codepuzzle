import Api from '../base-class';

class MetadataApi extends Api {
    controller = 'Metadata';
    
    retrieveMetadata = () => (
        this._get(this.controller + '/')
    )
}

export default new MetadataApi();