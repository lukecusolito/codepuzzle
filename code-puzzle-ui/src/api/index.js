import metadata from './endpoints/metadata';
import products from './endpoints/products';

export default {
    metadata,
    products
}