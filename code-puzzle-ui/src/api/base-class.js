class ApiClass {
    get _baseUrl () {
        return 'http://localhost:10104/api/'
    }

    _get = async (url) => {
        return new Promise((resolve, reject) => {

            try {
                const http = new XMLHttpRequest();
                
                http.open("GET", this._baseUrl + url, true);
                http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

                http.send();

                http.onload = () => {
                    if (http.readyState === 4) {
                        if (http.status === 200) {
                            resolve(JSON.parse(http.responseText));
                        } else {
                            reject({
                                status: http.status,
                                statusText: http.statusText
                            });
                        }
                    }
                }
            } 
            catch (error) {
                console.log(error);
            }
        });
    }
    
    _post = (url, body) => {
        return new Promise((resolve, reject) => {

            try {
                const http = new XMLHttpRequest();
                
                http.open("POST", this._baseUrl + url, true);
                http.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

                http.send(JSON.stringify(body));

                http.onload = () => {
                    if (http.readyState === 4) {
                        if (http.status === 200) {
                            resolve( http.responseText);
                        } else {
                            reject({
                                status: http.status,
                                statusText: http.statusText
                            });
                        }
                    }
                }
            } 
            catch (error) {
                console.log(error);
            }
        });
    }
}

export default ApiClass;