﻿using CodePuzzle.Controllers;
using CodePuzzle.Domain.Models.Configuration;
using CodePuzzle.Services.Interfaces.Services;
using Microsoft.Extensions.Options;
using NSubstitute;

namespace CodePuzzle.Test.Helpers.Controllers
{
    public class ProductControllerSetup
    {
        public ProductController Scope { get { return new ProductController(Mock_AppSettings, Mock_AllTheCloudsApiService); } }

        public IOptions<ApplicationSettings> Mock_AppSettings { get; set; } = Substitute.For<IOptions<ApplicationSettings>>();
        public IAllTheCloudsApiService Mock_AllTheCloudsApiService { get; set; } = Substitute.For<IAllTheCloudsApiService>();
    }
}
