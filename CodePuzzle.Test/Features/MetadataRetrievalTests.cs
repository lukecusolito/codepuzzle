﻿using CodePuzzle.Domain.Enumerations;
using CodePuzzle.Domain.Models;
using CodePuzzle.Domain.Models.Configuration;
using CodePuzzle.Test.Helpers.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace CodePuzzle.Test.Features
{
    /// <summary>
    /// @Feature: Metadata Retrieval
    /// </summary>
    /// <remarks>
    /// As a customer
    /// I want to access metadata
    /// So I am able to view the data correctly
    /// </remarks>
    [TestClass]
    public class MetadataRetrievalTests
    {
        /// <summary>
        /// @Scenario: Default exchange rate is set
        /// </summary>
        /// <remarks>
        /// Given a customer
        /// When I view the product list
        /// And the default exchange rate is configured
        /// Then I expect the default exchange rate to be displayed
        /// </remarks>
        [TestMethod, TestCategory("1.0.0.0")]
        public void DefaultExchangeRateIsSet()
        {
            // Arrange
            var expected = Currency.USD;

            var metadataController = new MetadataControllerSetup();
            metadataController.Mock_AppSettings.Value.Returns(new ApplicationSettings {DefaultCurrency = expected });

            // Act
            var actualResult = metadataController.Scope.GetMetadata().Result;

            // Assert
            var actual = AssertOkResult(actualResult);

            Assert.AreEqual(expected, actual.DefaultCurrency);
        }

        /// <summary>
        /// @Scenario: Exchange rates are set
        /// </summary>
        /// <remarks>
        /// Given a customer
        /// When I want to view the currencies and their exchange rates
        /// And current exchange rates are recieved
        /// Then I expect to recieve a list of exchange rates
        /// </remarks>
        [TestMethod, TestCategory("1.0.0.0")]
        public void ExchangeRatesAreSet()
        {
            // Arrange
            var expected = new List<ForeignExchangeRate>
            {
                new ForeignExchangeRate { SourceCurrency = "AUD", TargetCurrency = "USD", Rate = .25M },
                new ForeignExchangeRate { SourceCurrency = "USD", TargetCurrency = "AUD", Rate = .75M },
            };

            var metadataController = new MetadataControllerSetup();
            metadataController.Mock_AppSettings.Value.Returns(new ApplicationSettings());
            metadataController.Mock_AllTheCloudsApiService.Currency.GetForeignExchangeRates().Returns(expected);

            // Act
            var actualResult = metadataController.Scope.GetMetadata().Result;

            // Assert
            var actual = AssertOkResult(actualResult);

            Assert.AreEqual(expected.Count, actual.ExchangeRates.Count);

            foreach(var expectedRate in expected)
            {
                var actualRate = actual.ExchangeRates.Single(x => x.SourceCurrency == expectedRate.SourceCurrency);

                Assert.AreEqual(expectedRate.TargetCurrency, actualRate.TargetCurrency);
                Assert.AreEqual(expectedRate.Rate, actualRate.Rate);
            }

            metadataController.Mock_AllTheCloudsApiService.Currency.Received(1).GetForeignExchangeRates();
        }

        /// <summary>
        /// @Scenario: Exchange rates are not set when unavailable
        /// </summary>
        /// <remarks>
        /// Given a customer
        /// When I want to view the currencies and their exchange rates
        /// And current exchange rates are not recieved
        /// Then I expect not to recieve a list of exchange rates
        /// </remarks>
        [TestMethod, TestCategory("1.0.0.0")]
        public void ExchangeRatesAreNotSetWhenUnavailable()
        {
            // Arrange
            List<ForeignExchangeRate> expected = null;
            var metadataController = new MetadataControllerSetup();
            metadataController.Mock_AppSettings.Value.Returns(new ApplicationSettings());
            metadataController.Mock_AllTheCloudsApiService.Currency.GetForeignExchangeRates().Returns(expected);

            // Act
            var actualResult = metadataController.Scope.GetMetadata().Result;

            // Assert
            var actual = AssertOkResult(actualResult);

            Assert.IsNull(actual.ExchangeRates);

            metadataController.Mock_AllTheCloudsApiService.Currency.Received(1).GetForeignExchangeRates();
        }

        #region Private Methods
        private Metadata AssertOkResult(IActionResult result)
        {
            var objectResult = result as ObjectResult;

            Assert.IsNotNull(objectResult?.Value);
            Assert.AreEqual((int)HttpStatusCode.OK, objectResult.StatusCode);
            Assert.IsInstanceOfType(objectResult.Value, typeof(Metadata));

            return objectResult.Value as Metadata;
        }
        #endregion
    }
}
