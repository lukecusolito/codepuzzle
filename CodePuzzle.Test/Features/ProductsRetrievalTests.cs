﻿using CodePuzzle.Domain.Enumerations;
using CodePuzzle.Domain.Models;
using CodePuzzle.Domain.Models.Configuration;
using CodePuzzle.Test.Helpers.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace CodePuzzle.Test.Features
{
    /// <summary>
    /// @Feature: Products Retrieval
    /// </summary>
    /// <remarks>
    /// As a customer 
    /// I want to view a list of products with prices
    /// So that I can make informed decisions about what products to order
    /// </remarks>
    [TestClass]
    public class ProductsRetrievalTests
    {
        /// <summary>
        /// @Scenario: Products are recieved and markup is applied
        /// </summary>
        /// <remarks>
        /// Given a customer
        /// When the customer uses the application
        /// Then a list of products with descriptions and prices is displayed
        /// And the prices have a mark-up of 20% above the base price provided by the vendor
        /// </remarks>
        [TestMethod, TestCategory("1.0.0.0")]
        public void ProductsaAreRecievedAndMarkupIsApplied()
        {
            // Arrange
            var expectedMarkup = 0.20M;
            var expectedProductUnitPrice001 = 120M;
            var expectedProductUnitPrice002 = 54.82M;
            var expectedProductUnitPrice003 = 50677.35M;


            var products = new List<Product>
            {
                new Product { ProductId = "001", UnitPrice = 100M },
                new Product { ProductId = "002", UnitPrice = 45.68M },
                new Product { ProductId = "003", UnitPrice = 42231.12123M },
            };

            var productController = new ProductControllerSetup();
            productController.Mock_AppSettings.Value.Returns(new ApplicationSettings { ProductMarkup = expectedMarkup });
            productController.Mock_AllTheCloudsApiService.Product.Get().Returns(products);

            // Act
            var actualResult = productController.Scope.GetProducts().Result;

            // Assert
            var actual = AssertOkResult(actualResult);

            Assert.AreEqual(expectedProductUnitPrice001, actual.Single(x => x.ProductId == "001").UnitPrice);
            Assert.AreEqual(expectedProductUnitPrice002, actual.Single(x => x.ProductId == "002").UnitPrice);
            Assert.AreEqual(expectedProductUnitPrice003, actual.Single(x => x.ProductId == "003").UnitPrice);

            productController.Mock_AllTheCloudsApiService.Product.Received(1).Get();
        }

        /// <summary>
        /// @Scenario: Products are not recieved when products cant be loaded
        /// </summary>
        /// <remarks>
        /// Given a customer
        /// When the customer uses the application
        /// And products cannot be loaded
        /// Then I expect no products to be displayed
        /// And I expect no markup to be applied
        /// </remarks>
        [TestMethod, TestCategory("1.0.0.0")]
        public void ProductsaAreNotRecievedWhenProductsCantBeLoaded()
        {
            // Arrange
            var expected = ErrorCode.PRODUCTS_NOT_FOUND;
            List<Product> products = null;

            var productController = new ProductControllerSetup();
            productController.Mock_AllTheCloudsApiService.Product.Get().Returns(products);

            // Act
            var actualResult = productController.Scope.GetProducts().Result;

            // Assert
            var actual = AssertBadRequestResult(actualResult);

            Assert.IsNotNull(actual);
            Assert.AreEqual(expected, actual.Errors.First().ErrorCode);

            productController.Mock_AllTheCloudsApiService.Product.Received(1).Get();
        }

        #region Private Methods
        private List<Product> AssertOkResult(IActionResult result)
        {
            var objectResult = result as ObjectResult;

            Assert.IsNotNull(objectResult?.Value);
            Assert.AreEqual((int)HttpStatusCode.OK, objectResult.StatusCode);
            Assert.IsInstanceOfType(objectResult.Value, typeof(List<Product>));

            return objectResult.Value as List<Product>;
        }

        private ErrorResponse AssertBadRequestResult(IActionResult result)
        {
            var objectResult = result as ObjectResult;

            Assert.IsNotNull(objectResult?.Value);
            Assert.AreEqual((int)HttpStatusCode.BadRequest, objectResult.StatusCode);
            Assert.IsInstanceOfType(objectResult.Value, typeof(ErrorResponse));

            return objectResult.Value as ErrorResponse;
        }
        #endregion
    }
}
