﻿using CodePuzzle.Domain.Models.Configuration;
using CodePuzzle.Services.AllTheCloudsApiService;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CodePuzzle.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCodePuzzleMvc(this IServiceCollection services)
        {
            services
                .AddMvc()
                .AddJsonOptions(config =>
                {
                    config.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                });

            return services;
        }
        public static IServiceCollection AddCodePuzzleConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ApplicationSettings>(options => configuration.GetSection("AppSettings").Bind(options));
            services.Configure<AllTheCloudsApiConfiguration>(options => configuration.GetSection("AllTheCloudsApi").Bind(options));

            return services;
        }

        public static IServiceCollection AddCodePuzzleModules(this IServiceCollection services)
        {
            services.AddAllTheCloudsApiService();

            return services;
        }
    }
}
