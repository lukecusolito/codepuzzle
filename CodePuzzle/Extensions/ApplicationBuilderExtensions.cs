﻿using CodePuzzle.Middleware;
using Microsoft.AspNetCore.Builder;

namespace CodePuzzle.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseRequestMiddleware(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<RequestMiddleware>();

            return builder;
        }
    }
}
