﻿using CodePuzzle.Domain.Enumerations;
using CodePuzzle.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CodePuzzle.Middleware
{
    public class RequestMiddleware
    {
        #region Fields
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestMiddleware> _logger;
        #endregion

        #region Constructor
        public RequestMiddleware(RequestDelegate next, ILogger<RequestMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        #endregion

        #region Public Methods
        public async Task Invoke(HttpContext context)
        {
            if (!context.Request.Path.StartsWithSegments(new PathString("/api")))
            {
                await _next.Invoke(context);
                return;
            }

            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                await HandleExceptionAsync(context, ex);
            }
        }
        #endregion

        #region Private Methods
        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            // Override local settings, middleware unaffected by global configuration
            var jsonSettings = new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                Converters = new List<JsonConverter> { new Newtonsoft.Json.Converters.StringEnumConverter() }
            };

            var errorCode = ErrorCode.UNEXPECTED_ERROR;
            Enum.TryParse(exception.Message, out errorCode);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return context.Response.WriteAsync(JsonConvert.SerializeObject(new ErrorResponse(errorCode), jsonSettings));
        }
        #endregion
    }
}
