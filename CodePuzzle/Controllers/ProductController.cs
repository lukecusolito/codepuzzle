﻿using CodePuzzle.Domain.Enumerations;
using CodePuzzle.Domain.Models;
using CodePuzzle.Domain.Models.Configuration;
using CodePuzzle.Services.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;

namespace CodePuzzle.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        #region Fields
        private readonly IAllTheCloudsApiService _allTheCloudsApiService;
        private readonly ApplicationSettings _applicationSettings;
        #endregion

        #region Constructor
        public ProductController(IOptions<ApplicationSettings> applicationSettings, IAllTheCloudsApiService allTheCloudsApiService)
        {
            _applicationSettings = applicationSettings.Value;
            _allTheCloudsApiService = allTheCloudsApiService;
        }
        #endregion

        [HttpGet]
        public async Task<IActionResult> GetProducts()
        {
            var products = await _allTheCloudsApiService.Product.Get();

            products?
                .ToList()
                .ForEach(x => x.AddProductMarkup(_applicationSettings.ProductMarkup));

            if (products == null)
                return BadRequest(new ErrorResponse(ErrorCode.PRODUCTS_NOT_FOUND));

            return Ok(products);
        }
    }
}