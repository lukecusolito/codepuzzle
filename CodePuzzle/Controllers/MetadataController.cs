﻿using CodePuzzle.Domain.Models;
using CodePuzzle.Domain.Models.Configuration;
using CodePuzzle.Services.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;

namespace CodePuzzle.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MetadataController : Controller
    {
        #region Fields
        private readonly IAllTheCloudsApiService _allTheCloudsApiService;
        private readonly ApplicationSettings _applicationSettings;
        #endregion

        #region Constructor
        public MetadataController(IOptions<ApplicationSettings> applicationSettings, IAllTheCloudsApiService allTheCloudsApiService)
        {
            _allTheCloudsApiService = allTheCloudsApiService;
            _applicationSettings = applicationSettings.Value;
        }
        #endregion

        [HttpGet]
        public async Task<IActionResult> GetMetadata()
        {
            var metadata = new Metadata
            {
                DefaultCurrency = _applicationSettings.DefaultCurrency,
                ExchangeRates = (await _allTheCloudsApiService.Currency.GetForeignExchangeRates())?.ToList()
            };

            return Ok(metadata);
        }
    }
}