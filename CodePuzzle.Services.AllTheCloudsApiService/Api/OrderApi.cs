﻿using CodePuzzle.Domain.Models;
using CodePuzzle.Services.AllTheCloudsApiService.Helpers;
using CodePuzzle.Services.Interfaces.Api;
using System;
using System.Threading.Tasks;

namespace CodePuzzle.Services.AllTheCloudsApiService.Api
{
    public class OrderApi : IOrderApi
    {
        #region Fields
        private readonly IRestHelper _restHelper;
        #endregion

        #region Constructor
        public OrderApi(IRestHelper restHelper)
        {
            _restHelper = restHelper;
        }
        #endregion

        #region Public Methods
        public async Task<bool> SubmitOrder(Order order)
        {
            try
            {
                var result = await _restHelper.PostAsync("Orders", order);
            }
            catch (Exception) { }

            return false;
        }

        #endregion
    }
}
