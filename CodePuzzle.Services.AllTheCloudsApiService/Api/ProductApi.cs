﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CodePuzzle.Domain.Models;
using CodePuzzle.Services.AllTheCloudsApiService.Helpers;
using CodePuzzle.Services.Interfaces.Api;

namespace CodePuzzle.Services.AllTheCloudsApiService.Api
{
    public class ProductApi : IProductApi
    {
        #region Fields
        private readonly IRestHelper _restHelper;
        #endregion

        #region Constructor
        public ProductApi(IRestHelper restHelper)
        {
            _restHelper = restHelper;
        }
        #endregion

        #region Public Methods
        public async Task<IEnumerable<Product>> Get()
        {
            try
            {
                return await _restHelper.GetAsync<IEnumerable<Product>>("Products");
            }
            catch (Exception) { }

            return null;
        }

        #endregion
    }
}
