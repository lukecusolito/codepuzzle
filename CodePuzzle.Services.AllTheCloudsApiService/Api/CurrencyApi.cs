﻿using CodePuzzle.Domain.Models;
using CodePuzzle.Services.AllTheCloudsApiService.Helpers;
using CodePuzzle.Services.Interfaces.Api;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodePuzzle.Services.AllTheCloudsApiService.Api
{
    public class CurrencyApi : ICurrencyApi
    {
        #region Fields
        private readonly IRestHelper _restHelper;
        #endregion

        #region Constructor
        public CurrencyApi(IRestHelper restHelper)
        {
            _restHelper = restHelper;
        }
        #endregion

        #region Public Methods
        public async Task<IEnumerable<ForeignExchangeRate>> GetForeignExchangeRates()
        {
            try
            {
                return await _restHelper.GetAsync<IEnumerable<ForeignExchangeRate>>("fx-rates");
            }
            catch (Exception) { }

            return null;
        }

        #endregion
    }
}
