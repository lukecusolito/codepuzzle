﻿using CodePuzzle.Services.AllTheCloudsApiService.Api;
using CodePuzzle.Services.AllTheCloudsApiService.Helpers;
using CodePuzzle.Services.Interfaces.Api;
using CodePuzzle.Services.Interfaces.Services;

namespace CodePuzzle.Services.AllTheCloudsApiService
{
    public class AllTheCloudsApiService : IAllTheCloudsApiService
    {
        #region Fields
        private readonly IRestHelper _restHelper;

        private ICurrencyApi _currencyApi;
        private IOrderApi _orderApi;
        private IProductApi _productApi;
        #endregion

        #region Constructor
        public AllTheCloudsApiService(IRestHelper restHelper)
        {
            _restHelper = restHelper;
        }
        #endregion

        #region Properties
        public ICurrencyApi Currency
        {
            get
            {
                if (_currencyApi == null)
                    _currencyApi = new CurrencyApi(_restHelper);

                return _currencyApi;
            }
        }

        public IOrderApi Order
        {
            get
            {
                if (_orderApi == null)
                    _orderApi = new OrderApi(_restHelper);

                return _orderApi;
            }
        }

        public IProductApi Product
        {
            get
            {
                if (_productApi == null)
                    _productApi = new ProductApi(_restHelper);

                return _productApi;
            }
        }
        #endregion
    }
}
