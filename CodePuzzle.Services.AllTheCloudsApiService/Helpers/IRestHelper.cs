﻿using System.Threading.Tasks;

namespace CodePuzzle.Services.AllTheCloudsApiService.Helpers
{
    public interface IRestHelper
    {
        Task<T> GetAsync<T>(string path);
        Task<bool> PostAsync(string path, object requestContent);
        Task<T> PostAsync<T>(string path, object requestContent);
    }
}
