﻿using CodePuzzle.Domain.Models.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CodePuzzle.Services.AllTheCloudsApiService.Helpers
{
    public class RestHelper : IRestHelper
    {
        #region Fields
        private readonly HttpClient httpClient;
        private readonly AllTheCloudsApiConfiguration _configuration;
        #endregion

        #region Constructor
        public RestHelper(IOptions<AllTheCloudsApiConfiguration> configuration)
        {
            _configuration = configuration.Value;

            httpClient = new HttpClient();

            httpClient.DefaultRequestHeaders
                .Accept
                .Add(new MediaTypeWithQualityHeaderValue("application/json"));

            httpClient.DefaultRequestHeaders
                .Add("api-key", _configuration.ApiKey);

            httpClient.BaseAddress = new Uri(_configuration.BaseUrl);
        }
        #endregion

        #region GET
        public async Task<T> GetAsync<T>(string path)
        {
            var result = await httpClient.GetAsync(path);

            return await result.Content.ReadAsAsync<T>();
        }
        #endregion

        #region POST
        public async Task<bool> PostAsync(string path, object requestContent)
        {
            var result = await httpClient.PostAsync(path, requestContent, new JsonMediaTypeFormatter());

            return result.IsSuccessStatusCode;
        }

        public async Task<T> PostAsync<T>(string path, object requestContent)
        {
            var result = await httpClient.PostAsync(path, requestContent, new JsonMediaTypeFormatter());

            return await result.Content.ReadAsAsync<T>();
        }
        #endregion
    }
}
