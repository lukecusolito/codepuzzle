﻿using CodePuzzle.Services.AllTheCloudsApiService.Api;
using CodePuzzle.Services.AllTheCloudsApiService.Helpers;
using CodePuzzle.Services.Interfaces.Api;
using CodePuzzle.Services.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;

namespace CodePuzzle.Services.AllTheCloudsApiService
{
    public static class AllTheCloudsApiModule
    {
        public static IServiceCollection AddAllTheCloudsApiService(this IServiceCollection services)
        {
            services.AddDependencies();

            return services;
        }

        private static IServiceCollection AddDependencies(this IServiceCollection services)
        {
            services.AddScoped<IRestHelper, RestHelper>();
            services.AddTransient<IAllTheCloudsApiService, AllTheCloudsApiService>();
            services.AddTransient<ICurrencyApi, CurrencyApi>();
            services.AddTransient<IOrderApi, OrderApi>();
            services.AddTransient<IProductApi, ProductApi>();

            return services;
        }
    }
}
